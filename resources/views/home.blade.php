<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="123">

    <title>Tracking</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
        <script src="/js/app.js" defer="defer"></script>
    
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            Tracking
        </a>

    </div>
</nav>
        <div id="app" class="flex-center position-ref full-height">
            <main class="py-4">
            <example-component></example-component>
        </main>
        </div>
    </body>
</html>
