<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $appends = [
        'city_center',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function clients()
    {
        return $this->hasMany(Client::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getCityCenterAttribute()
    {
        return [
            'coordinates' => [
                $this->lat,
                $this->lng,
            ]
        ];
    }
}
