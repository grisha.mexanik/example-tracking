<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
    ];

    protected $hidden = [
        'lat',
        'lng',
    ];


    protected $appends = [
        'latest_position',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    public function getLatestPositionAttribute()
    {
        return $this->positions()->latest('created_at')->first();
    }
}
