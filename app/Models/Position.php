<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $appends = [
        'position',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    
    public function getPositionAttribute()
    {
        return [
            'coordinates' => [
                $this->lat,
                $this->lng,
            ]
        ];
    }
}
