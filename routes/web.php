<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
	// $points = simplexml_load_file(storage_path("app/muc1.gpx"))->trk->trkseg;
	// $client = App\Models\Client::find(5);

	// $time = Carbon\Carbon::now();

	// foreach ($points->trkpt as $point) {
	// 	$position = new App\Models\Position();
	// 	$position->client_id = $client->id;
	// 	$position->lat = $point->attributes()->lat;
	// 	$position->lng = $point->attributes()->lon;

	// 	$position->created_at = $time;
	// 	$position->updated_at = $time;

	// 	$time = $time->addMinute();

	// 	$position->save();
	// }

    return view('home');
});

$router->get('/api/data', function () use ($router) {
	return App\Models\Country::with('cities.clients.city')->get();
});

$router->get('/api/client', function () use ($router) {
	return App\Models\Client::with('city')->get();
});

$router->get('/api/position/{client}', function ($client) use ($router) {
	return App\Models\Client::find($client)->positions()->whereDate('updated_at', Carbon\Carbon::today())->get();
});