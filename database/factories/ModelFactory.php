<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Clients::class, function (Faker\Generator $faker) {
    return [
        'fullname' => $faker->person->name,
        'email' => $faker->internet->email,
        'phone' => $faker->phone->e164PhoneNumber,
    ];
});
