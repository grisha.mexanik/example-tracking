<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clients')->delete();
        
        \DB::table('clients')->insert(array (
            0 => 
            array (
                'id' => 1,
                'city_id' => 1,
                'fullname' => 'Клиент 1',
                'email' => 'client1@example.com',
                'phone' => '+79999999901',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'city_id' => 1,
                'fullname' => 'Клиент 2',
                'email' => 'client2@example.com',
                'phone' => '+79999999902',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'city_id' => 2,
                'fullname' => 'Клиент 3',
                'email' => 'client3@example.com',
                'phone' => '+79999999903',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'city_id' => 2,
                'fullname' => 'Клиент 4',
                'email' => 'client4@example.com',
                'phone' => '+79999999904',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'city_id' => 3,
                'fullname' => 'Клиент 5',
                'email' => 'client5@example.com',
                'phone' => '+79999999905',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}