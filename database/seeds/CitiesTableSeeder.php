<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cities')->delete();
        
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'country_id' => 1,
                'name' => 'Moscow',
                'lat' => 55.75583,
                'lng' => 37.61778,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'country_id' => 1,
                'name' => 'Ekaterinburg',
                'lat' => 56.83333,
                'lng' => 60.58333,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'country_id' => 2,
                'name' => 'Munich',
                'lat' => 48.137194,
                'lng' => 11.5755,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}